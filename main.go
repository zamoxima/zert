package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
)

type CertShResponse struct {
	IssuerId          int    `json:"issuer_ca_id"`
	IssuerName        string `json:"issuer_name"`
	Name              string `json:"name_value"`
	MinCertId         int    `json:"min_cert_id"`
	MinEntryTimestamp string `json:"min_entry_timestamp"`
	NotBefore         string `json:"not_before"`
	NofAfter          string `json:"not_after"`
}

type CertSpotterResponse struct {
	Id        string   `json:"id"`
	TBS       string   `json:"tbs_sha256"`
	DNSNames  []string `json:"dns_names"`
	PubKey    string   `json:"pubkey_sha256"`
	NotBefore string   `json:"not_before"`
	NotAfter  string   `json:"not_after"`
}

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("Usage:\n  %s [domain]\n", os.Args[0])
		return
	}

	domain := os.Args[1]

	subdomains := append(queryCertSpotter(domain), queryCrtSh("%."+domain)...)
	subdomains = removeDuplicates(subdomains)

	for _, d := range subdomains {
		fmt.Println(d)
	}
}

func queryCrtSh(query string) []string {
	var subdomains []string
	escapedQuery := url.QueryEscape(query)
	url := fmt.Sprintf("https://crt.sh/?q=%s&output=json", escapedQuery)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal("NewRequest: ", err)
		return subdomains
	}

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
		return subdomains
	}

	defer resp.Body.Close()

	var certificates []CertShResponse

	if err := json.NewDecoder(resp.Body).Decode(&certificates); err != nil {
		log.Fatal("Decode: ", err)
		return subdomains
	}

	for _, cert := range certificates {
		subdomains = append(subdomains, cert.Name)
	}

	return subdomains
}

func queryCertSpotter(domain string) []string {
	var subdomains []string
	escapedDomain := url.QueryEscape(domain)
	url := fmt.Sprintf("https://api.certspotter.com/v1/issuances?domain=%s&include_subdomains=true&expand=dns_names", escapedDomain)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal("NewRequest: ", err)
		return subdomains
	}

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
		return subdomains
	}

	defer resp.Body.Close()

	var certificates []CertSpotterResponse

	if err := json.NewDecoder(resp.Body).Decode(&certificates); err != nil {
		log.Fatal("Decode: ", err)
		return subdomains
	}

	for _, cert := range certificates {
		for _, name := range cert.DNSNames {
			if strings.HasSuffix(name, domain) {
				subdomains = append(subdomains, name)
			}
		}
	}

	return subdomains
}

func removeDuplicates(elements []string) []string {
	encountered := map[string]bool{}
	result := []string{}

	for v := range elements {
		if encountered[elements[v]] != true {
			encountered[elements[v]] = true
			result = append(result, elements[v])
		}
	}

	return result
}
